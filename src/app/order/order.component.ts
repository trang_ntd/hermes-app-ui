import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import * as $ from 'jquery';
import {tweens, hitAreas} from 'ngvas';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, AfterViewInit {
  selectedRoute = 1;
  routes = [
    {
      id: 1,
      positions: [
        {
          x: 334,
          y: 293,
        },
        {
          x: 335,
          y: 321,
        },
        {
          x: 348,
          y: 341,
        },
        {
          x: 377,
          y: 346,
        },
        {
          x: 374,
          y: 376,
        },
        {
          x: 381,
          y: 399,
        },
        {
          x: 399,
          y: 416,
        },
        {
          x: 426,
          y: 434,
        },
        {
          x: 458,
          y: 433,
        },
        {
          x: 493,
          y: 447,
        },
        {
          x: 504,
          y: 499,
        },
        {
          x: 486,
          y: 524,
        },
        {
          x: 450,
          y: 548,
        },
      ]
    },
    {
      id: 2,
      positions: [
        {
          x: 334,
          y: 293,
        },
        {
          x: 315,
          y: 306,
        },
        {
          x: 290,
          y: 324,
        },
        {
          x: 256,
          y: 332,
        },
        {
          x: 232,
          y: 318,
        },
        {
          x: 198,
          y: 283,
        },
        {
          x: 196,
          y: 328,
        },
        {
          x: 223,
          y: 343,
        },
        {
          x: 242,
          y: 360,
        },
        {
          x: 250,
          y: 384,
        },
        {
          x: 269,
          y: 409,
        },
        {
          x: 297,
          y: 419,
        },
        {
          x: 327,
          y: 437,
        },
        {
          x: 352,
          y: 455,
        },
        {
          x: 383,
          y: 466,
        },
        {
          x: 410,
          y: 474,
        },
        {
          x: 428,
          y: 495,
        },
        {
          x: 440,
          y: 521,
        },
        {
          x: 448,
          y: 552,
        },
      ]
    }
  ];

  ngOnInit() {
    $('.right').bind('click', ev => {
      var $div = $(ev.target);
      // var $display = $div.find('.display');
      var offset = $div.offset();
      var x = ev.clientX - offset.left;
      var y = ev.clientY - offset.top;

      console.log('x: ' + x + ', y: ' + y);
    });
  }

  createLine(pos1, pos2, lineId) {
    const distance = Math.sqrt((pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y));

    const xMid = (pos1.x + pos2.x) / 2;
    const yMid = (pos1.y + pos2.y) / 2;

    const salopeInradian = Math.atan2(pos1.y - pos2.y, pos1.x - pos2.x);
    const salopeInDegress = (salopeInradian * 180) / Math.PI;

    const line = document.getElementById(lineId);
    line.style.width = distance + 'px';
    line.style.top = yMid + 'px';
    line.style.left = (xMid - (distance / 2)) + 'px';
    line.style.transform = 'rotate(' + salopeInDegress + 'deg)';
  }

  selectRoute(id: number) {
    this.selectedRoute = id;
  }

  ngAfterViewInit() {
    this.routes.forEach((route, index) => {
      for (let i = 0; i < route.positions.length - 1; i++) {
        this.createLine(route.positions[i], route.positions[i + 1], 'route' + index + 'line' + i);
      }
    });
  }
}
